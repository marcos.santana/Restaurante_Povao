package br.ucsal.bes.poo20181.restaurante.domain;

public class Teste {

	public static void main(String[] args) {
		Restaurante restaurante = new Restaurante();

		restaurante.adicionarItem(new Item("Churrasco", "Comida boa", 100));
		restaurante.adicionarItem(new Item("Feijoada", "Comida boa", 100));
		restaurante.adicionarItem(new Item("Lasanha", "Comida boa", 100));
		restaurante.adicionarItem(new Item("Moqueca", "Comida boa", 100));
		restaurante.adicionarItem(new Item("Arroz", "Comida boa", 100));

		restaurante.adicionarMesa(new Mesa(4));
		restaurante.adicionarMesa(new Mesa(8));
		restaurante.adicionarMesa(new Mesa(2));
		restaurante.adicionarMesa(new Mesa(10));

		restaurante.ocuparMesa(4);
		restaurante.ocuparMesa(10);

		for (int i = 0; i < restaurante.getMesa().size(); i++) {
			System.out.println("A mesa " + restaurante.getMesa().get(i).getNumero() + " est� "
					+ restaurante.getMesa().get(i).getEstado().getDescricao());
		}

//		for (int i = 0; i < restaurante.getItem().size(); i++) {
//			System.out.println(restaurante.getItem().get(i).getNome());
//		}
		
		restaurante.higienizarMesa(false, 4);
		
		for (int i = 0; i < restaurante.getMesa().size(); i++) {
			System.out.println("A mesa " + restaurante.getMesa().get(i).getNumero() + " est� "
					+ restaurante.getMesa().get(i).getEstado().getDescricao());
		}
		restaurante.liberarMesa(4);
		
		for (int i = 0; i < restaurante.getMesa().size(); i++) {
			System.out.println("A mesa " + restaurante.getMesa().get(i).getNumero() + " est� "
					+ restaurante.getMesa().get(i).getEstado().getDescricao());
		}
	}

}
