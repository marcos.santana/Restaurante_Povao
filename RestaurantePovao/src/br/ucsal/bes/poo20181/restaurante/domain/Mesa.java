package br.ucsal.bes.poo20181.restaurante.domain;

public class Mesa {

	private static int contador;
	private Integer numero;
	private Integer capacidade;
	private Estado estado;

	public Mesa(Integer capacidade) {
		obterNumero();
		this.estado = Estado.LIBERADA;
		this.capacidade = capacidade;
	}

	private void obterNumero() {
		contador++;
		numero = contador;
	}

	public Integer getCapacidade() {
		return capacidade;
	}

	public Integer getNumero() {
		return numero;
	}
	public void setEstado(Estado estado) {
		this.estado=estado;
	}
	public Estado getEstado() {
		return estado;
	}

}
