package br.ucsal.bes.poo20181.restaurante.domain;

public class Item {
	private static int contador;

	private Integer codigo;
	private String nome;
	private String descricao;
	private double valor;

	public Item(String nome, String descricao, double valor) {
		obterCodigo();
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Integer getCodigo() {
		return codigo;
	}

	private void obterCodigo() {
		contador++;
		this.codigo = contador;
	}

}
