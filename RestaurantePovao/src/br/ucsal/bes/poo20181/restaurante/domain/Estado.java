package br.ucsal.bes.poo20181.restaurante.domain;

public enum Estado {

	OCUPADA("Ocupada"), LIBERADA("Liberada"), MANUTENCAO("Manutencao");

	private String descricao;

	Estado(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
