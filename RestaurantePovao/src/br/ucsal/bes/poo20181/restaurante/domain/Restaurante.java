package br.ucsal.bes.poo20181.restaurante.domain;

import java.util.ArrayList;

public class Restaurante {
	private ArrayList<Item> item;
	private ArrayList<Mesa> mesa;

	public Restaurante() {
		item = new ArrayList<>();
		mesa = new ArrayList<>();
	}
	
	public ArrayList<Item> getItem() {
		return item;
	}
	public ArrayList<Mesa> getMesa() {
		return mesa;
	}
	
	public void adicionarItem(Item item) {
		this.item.add(item);
	}
	public void adicionarMesa(Mesa mesa) {
		this.mesa.add(mesa);
	}
	
	public void ocuparMesa(int quantidade) {
		for(int i=0; i<mesa.size();i++) {
			if(mesa.get(i).getCapacidade()>=quantidade&&mesa.get(i).getEstado()==Estado.LIBERADA) {
				mesa.get(i).setEstado(Estado.OCUPADA);
				System.out.println("A mesa"+" "+mesa.get(i).getNumero()+" est� "+mesa.get(i).getEstado().getDescricao());
				break;
			}
		}
	}
	public void higienizarMesa(boolean cliente, int numMesa) {
		if(!cliente) {
			mesa.get(numMesa-1).setEstado(Estado.MANUTENCAO);
			System.out.println("A mesa " + mesa.get(numMesa-1).getNumero()+" est� " + mesa.get(numMesa-1).getEstado().getDescricao());
		}
	}
	public void liberarMesa(int numMesa) {
		if(mesa.get(numMesa-1).getEstado()==Estado.MANUTENCAO) {
			mesa.get(numMesa-1).setEstado(Estado.LIBERADA);
		}
	}

}
